class MyMap {
	constructor() {
		this.latitude = 0;
		this.longitude = 0;
		this.minValue = 0;
		this.maxValue = 5;
		this.markers = [];
	}
	
	init(){
		let geo = navigator.geolocation.getCurrentPosition((geo) => {
			let crd = geo.coords;
			this.latitude = crd.latitude;
			this.longitude = crd.longitude;

			let pos = {
				lat: this.latitude,
				lng: this.longitude
			};

			this.map.setCenter(pos);

			const marker = new google.maps.Marker({
				position: pos,
				map: this.map,
				title: "My position",
			});

		}, () => {
			console.log(`ERREUR (${err.code}): ${err.message}`);
		});


		const position = { lat: -25.344, lng: 131.036 };
		this.map = new google.maps.Map(document.querySelector(".map"), {
			zoom: 9,
			center: { lat: -34.397, lng: 150.644 },
		});

		let min = document.querySelector("#min");
		let max = document.querySelector("#max");
	
		min.addEventListener('change', (event) => {
			this.minValue = min.value;
			this.renderList();
			this.addMarkers();
		})
		max.addEventListener('change', (event) => {
			this.maxValue = max.value;
			this.renderList();
			this.addMarkers();
		})
		
	}

	renderList() {

		let ulList = document.querySelector(".list ul");
		ulList.innerHTML = "";

		fetch("./restaurants.json")
		.then(res => res.json())
		.then((restaurants) => {
			restaurants.forEach(restaurant => {
				let li = document.createElement("li");
				let commentaire = "";
				let avgMark = 0;
				
				restaurant.ratings.forEach(avis => {
					avgMark += avis.stars;
					commentaire += `Note : ${avis.stars} <br> Commentaire : ${avis.comment}<br><br>`;
				});
				
				avgMark = avgMark / restaurant.ratings.length;
				
				if (this.minValue <= avgMark && this.maxValue >= avgMark) {
					li.insertAdjacentHTML('afterbegin',` Nom du restaurant : ${restaurant.restaurantName} <br><br> Adresse : ${restaurant.address}<br><br> <div id="${restaurant.id}" class= "commentaires">${commentaire}</div>`);
					ulList.appendChild(li);
				}
					
			});
		});
	}

	removeMarkers() {
		this.markers.forEach((marker) => {
			marker.setMap(null);
		});
	}

	addMarkers() {

		this.removeMarkers();

		fetch("./restaurants.json")
		.then(res => res.json())
		.then((restaurants) => {
			restaurants.forEach(restaurant => {
				
				let avgMark = 0;
				restaurant.ratings.forEach(avis => {
					avgMark += avis.stars;
				});
		
				avgMark = avgMark / restaurant.ratings.length;

				if (this.minValue <= avgMark && this.maxValue >= avgMark) {
					let marker = new google.maps.Marker({
						position: { lat: restaurant.lat, lng: restaurant.long },
						map: this.map,
						title: restaurant.restaurantName,
						id: restaurant.id,
					});

					marker.addListener("click", () => {
						let panorama = new google.maps.StreetViewPanorama(
							document.getElementById("streetView"),
							{
								position: { lat: restaurant.lat, lng: restaurant.long },
								pov: { heading: 14, pitch: 10 },
							}
						);

						this.map.setStreetView(panorama);
						let comments = document.querySelectorAll("div.commentaires");
						
						comments.forEach(commentId => {
							commentId.style.display = "none";

							if (commentId.id == marker.id) {
								commentId.style.display = "block";
							}
						}); 
					});

					this.markers.push(marker);
				}
			});
		});
	}

}

const appMap = new MyMap();

function initMap() {
	appMap.init();
	appMap.renderList();
	appMap.addMarkers();


};